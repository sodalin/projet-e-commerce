import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sigle-product',
  templateUrl: './sigle-product.component.html',
  styleUrls: ['./sigle-product.component.scss']
})
export class SigleProductComponent implements OnInit {

  @Input() id : number;
  @Input()category: string;
  @Input() image : string;
  @Input() name : string;
  @Input() price : number;
  @Input() description: string;
  @Input() btn:string;
 
  // image = "../assets/slide2.png";
  // name = "Crème de nuit";
  // price = "$20";
  // description = "Pour la peaux sèche";




  constructor() { }

  ngOnInit() {
  }

}
