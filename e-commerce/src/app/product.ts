export interface Product {
    id: number,
    category: string,
    image :string,
    name: string,
    price: number,
    description: string,
    btn: string,       
    
}
