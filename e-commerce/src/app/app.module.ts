import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppComponent } from './app.component';
// import materialize
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatRadioModule} from '@angular/material/radio';
import { NavbarComponent } from './navbar/navbar.component';
import { ViewProductsComponent } from './view-products/view-products.component';
import { RouterModule, Routes } from '@angular/router';
import { ProductService } from './product.service';
import { SigleProductComponent } from './sigle-product/sigle-product.component';


const appRoutes: Routes = [
  
  { path: 'product', component: ViewProductsComponent},
  { path: '', component: ViewProductsComponent},
  { path: 'product/:category', component: ViewProductsComponent},
  
];



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ViewProductsComponent,
    SigleProductComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatRadioModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
