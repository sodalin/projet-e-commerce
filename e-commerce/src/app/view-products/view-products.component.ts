import { Component, OnInit, DoCheck } from '@angular/core';
import { ProductService } from '../product.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-products',
  templateUrl: './view-products.component.html',
  styleUrls: ['./view-products.component.scss']
})
export class ViewProductsComponent implements OnInit, DoCheck{

  products: any[] = [];
  category: string;
  image: string;
  
  constructor(private productService: ProductService,private route: ActivatedRoute) { }

  ngOnInit() {
    this.category = this.route.snapshot.params['category']
    this.products = this.productService.products;
    this.productService.ProductSubject.subscribe(
      (data) => this.products = data
    );
  }

  ngDoCheck() {
    console.log('ngDoCheck')
    this.category= this.route.snapshot.params['category'] || 'default';
    this.image = this.productService.banners[this.category]
    if(this.category != 'default') {
      this.productService.getProductByCategory(this.category);
    }   
   }
  
 
}
