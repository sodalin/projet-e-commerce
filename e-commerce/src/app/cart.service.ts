import { Injectable } from '@angular/core';
import { Product } from './product';

@Injectable()
export class CartService {

  items: number = 0;
  total: number = 0;
  products: Product[]

  constructor() { }

  addProduct(product) {
    this.products.push(product)
    this.items = this.products.length
  }
  
}

