import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
   //  alerte de payement 
   alertClick() {
    let alert = $('.alert');
    alert.slideDown();
    console.log(alert);
    setTimeout(() => { alert.slideUp() }, 4000);
  }
  

}
