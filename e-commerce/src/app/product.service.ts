import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class ProductService {
  
  public ProductSubject: Subject<any> = new Subject();

  public Product: any[] = [];

  getEmitProduct() {
    this.ProductSubject.next(this.Product)
  }

  getProductByCategory(category) {
    this.Product = this.products.filter((pro) =>{
      return pro.category == category;
     });
     this.getEmitProduct();
  }

  constructor() { 
    this.Product = this.products;
  }



  banners = {
    'default': 'slide-home.png',
    'cheveux': 'slide-cheuveux.jpg',
    'visage': 'slide-visage.jpg',
    'corps': 'slide-corps.png',
    'bebe': 'slide-baby.jpg',
   
  }


  products = [
    {
      id : 1,
      category: "cheveux",
      image : "../assets/cheveux1.jpg",
      name : "Crème jour",
      price : "$100",
      description : "Pour les cheveux secs",
      btn : 'Acheter',
    },
    {
      id : 2,
      category: "cheveux",
      image : "../assets/cheveux2.png",
      name : "Crème soin",
      price : "$50",
      description : "Pour les cheveux secs",
      btn : 'Acheter',
    },
    {
      id : 3,
      category: "cheveux",
      image : "../assets/cheveux3.jpg",
      name : "Crème jour",
      price : "$100",
      description : "Pour les cheveux gras",
      btn : 'Acheter',
    },
    {
      id : 4,
      category: "cheveux",
      image : "../assets/cheveux4.jpg",
      name : "Crème soin",
      price : "$50",
      description : "Pour les cheveux gras",
      btn : 'Acheter',
    },
    {
      id : 5,
      category: "cheveux",
      image : "../assets/cheveux5.jpg",
      name : "Crème jour",
      price : "$100",
      description : "Pour les cheveux normal",
      btn : 'Acheter',
    },
    {
      id : 6,
      category: "cheveux",
      image : "../assets/cheveux6.jpg",
      name : "Crème soin",
      price : "$50",
      description : "Pour les cheveux normal",
      btn : 'Acheter',
    },
    {
      id : 7,
      category: "visage",
      image : "../assets/visage1.jpg",
      name : "Crème jour",
      price : "$100",
      description : "Pour les peaux normal",
      btn : 'Acheter',
    },
    {
      id : 8,
      category: "visage",
      image : "../assets/visage2.jpg",
      name : "Crème soin",
      price : "$50",
      description : "Pour la peaux desydraté",
      btn : 'Acheter',
    },
    {
      id : 9,
      category: "visage",
      image : "../assets/visage3.jpg",
      name : "Crème jour",
      price : "$100",
      description : "Pour la peaux shèche",
      btn : 'Acheter',
    },
    {
      id : 10,
      category: "visage",
      image : "../assets/visage4.jpg",
      name : "Crème soin",
      price : "$50",
      description : "Pour la peaux gras",
      btn : 'Acheter',
    },
    {
      id : 11,
      category: "visage",
      image : "../assets/visage5.jpg",
      name : "Crème jour",
      price : "$100",
      description : "Pour la peaux desydraté",
      btn : 'Acheter',
    },
    {
      id : 12,
      category: "visage",
      image : "../assets/visage6.jpg",
      name : "Crème soin",
      price : "$50",
      description : "Pour la peaux normal",
      btn : 'Acheter',
    },

    {
      id : 13,
      category: "corps",
      image : "../assets/corp1.jpg",
      name : "Crème jour",
      price : "$100",
      description : "Pour la peaux desydraté",
      btn : 'Acheter',
    },
    {
      id : 14,
      category: "corps",
      image : "../assets/corp2.jpg",
      name : "Crème soin",
      price : "$50",
      description : "Pour la peaux desydraté",
      btn : 'Acheter',
    },

    {
   id : 15,
   category: "bebe",
   image : "../assets/bebe1.jpg",
   name : "Crème de nuit",
   price : "$20",
   description : "Pour la peaux normal",
   btn : 'Acheter',
    },

    {
      id : 16,
      category: "bebe",
      image : "../assets/bebe2.jpg",
      name : "Gel douche",
      price : "$10",
      description : "Pour la peaux desydraté",
      btn : 'Acheter',
    },
    {
    id : 17,
    category: "bebe",
    image : "../assets/bebe3.jpg",
    name : "Crème de nuit",
    price : "$20",
    description : "Pour la peaux sèche",
    btn : 'Acheter',
     },
 
     {
       id : 18,
       category: "bebe",
       image : "../assets/bebe4.jpg",
       name : "Gel douche",
       price : "$10",
       description : "Pour la peaux desydraté",
       btn : 'Acheter',
     },
     {
     id : 19,
     category: "bebe",
     image : "../assets/bebe5.jpg",
     name : "Crème de nuit",
     price : "$20",
     description : "Pour la peaux sèche",
     btn : 'Acheter',
      },
  
      {
        id : 20,
        category: "bebe",
        image : "../assets/bebe6.jpg",
        name : "Gel douche",
        price : "$10",
        description : "Pour la peaux desydraté",
        btn : 'Acheter',
      }
    

    
  ]




  
  
}
